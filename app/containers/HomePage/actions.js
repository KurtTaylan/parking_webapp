/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  RETRIEVE_PARKING,
  RETRIEVE_PARKING_SUCCESS,
  RETRIEVE_PARKING_ERROR,

  CREATE_PARKINGLOT,
  CREATE_PARKINGLOT_SUCCESS,
  CREATE_PARKINGLOT_ERROR,

  SEARCH_PARKINGLOT,
  SEARCH_PARKINGLOT_SUCCESS,
  SEARCH_PARKINGLOT_ERROR,

  RETRIEVE_RESERVATIONS,
  RETRIEVE_RESERVATIONS_SUCCESS,
  RETRIEVE_RESERVATIONS_ERROR,

  CREATE_RESERVATION,
  CREATE_RESERVATION_SUCCESS,
  CREATE_RESERVATION_ERROR,

  DELETE_ALL_RESERVATION,
  DELETE_ALL_RESERVATION_SUCCESS,
  DELETE_ALL_RESERVATION_ERROR
} from './constants';

export function retrieveParking() {
  return {
    type: RETRIEVE_PARKING,
  };
}

export function retrieveParkingSuccess(retrieveParking) {
  return {
    type: RETRIEVE_PARKING_SUCCESS,
    retrieveParking
  };
}

export function retrieveParkingError(error) {
  return {
    type: RETRIEVE_PARKING_ERROR,
    error,
  };
}

export function createParkinglot(createParkingLot) {
  return {
    type: CREATE_PARKINGLOT,
    ...createParkingLot
  };
}

export function createParkinglotSuccess(createParkingLot) {
  return {
    type: CREATE_PARKINGLOT_SUCCESS,
    createParkingLot
  };
}

export function createParkinglotError(error) {
  return {
    type: CREATE_PARKINGLOT_ERROR,
    error
  };
}

/**
 * TBD
 */
export function searchParkinglot(searchParkinglot) {
  return {
    type: SEARCH_PARKINGLOT,
    ...searchParkinglot
  };
}

/**
 * TBD
 */
export function searchParkinglotSuccess(searchParkingLot) {
  return {
    type: SEARCH_PARKINGLOT_SUCCESS,
    searchParkingLot,
  };
}

/**
 * TBD
 */
export function searchParkinglotError(error) {
  return {
    type: SEARCH_PARKINGLOT_ERROR,
    error,
  };
}


/**
 * TBD
 */
export function retrieveReservation() {
  return {
    type: RETRIEVE_RESERVATIONS,
  };
}

/**
 * TBD
 */
export function retrieveReservationSuccess(retrieveReservation) {
  return {
    type: RETRIEVE_RESERVATIONS_SUCCESS,
    retrieveReservation
  };
}

/**
 * TBD
 */
export function retrieveReservationError(error) {
  return {
    type: RETRIEVE_RESERVATIONS_ERROR,
    error
  };
}

/**
 * TBD
 */
export function createReservation(createReservation) {
  return {
    type: CREATE_RESERVATION,
    ...createReservation
  };
}

/**
 * TBD
 */
export function createReservationSuccess(createReservation) {
  return {
    type: CREATE_RESERVATION_SUCCESS,
    createReservation,
  };
}


/**
 * TBD
 */
export function createReservationError(error) {
  return {
    type: CREATE_RESERVATION_ERROR,
    error,
  };
}

/**
 * TBD
 */
export function deleteAllReservation() {
  return {
    type: DELETE_ALL_RESERVATION,
  };
}

/**
 * TBD
 */
export function deleteAllReservationSuccess(deleteAllReservation) {
  return {
    type: DELETE_ALL_RESERVATION_SUCCESS,
    deleteAllReservation
  };
}

/**
 * TBD
 */
export function deleteAllReservationError(error) {
  return {
    type: DELETE_ALL_RESERVATION_ERROR,
    error
  };
}
