/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  RETRIEVE_PARKING,
  RETRIEVE_PARKING_SUCCESS,
  RETRIEVE_PARKING_ERROR,

  CREATE_PARKINGLOT,
  CREATE_PARKINGLOT_SUCCESS,
  CREATE_PARKINGLOT_ERROR,

  SEARCH_PARKINGLOT,
  SEARCH_PARKINGLOT_SUCCESS,
  SEARCH_PARKINGLOT_ERROR,

  RETRIEVE_RESERVATIONS,
  RETRIEVE_RESERVATIONS_SUCCESS,
  RETRIEVE_RESERVATIONS_ERROR,

  CREATE_RESERVATION,
  CREATE_RESERVATION_SUCCESS,
  CREATE_RESERVATION_ERROR,

  DELETE_ALL_RESERVATION,
  DELETE_ALL_RESERVATION_SUCCESS,
  DELETE_ALL_RESERVATION_ERROR
} from './constants';

// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  errorMessage: '',
  parking: {
    search: {},
    create: {},
    list: []
  },
  reservation: {
    search: {},
    create: {},
    list: []
  }
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CREATE_PARKINGLOT:
        draft.loading = true;
        draft.error = false;
        draft.errorMessage = '';
        draft.parking.create = {}
        break;
      case CREATE_PARKINGLOT_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.errorMessage = '';
        draft.parking.create = action.createParkingLot;
        break;
      case CREATE_PARKINGLOT_ERROR:
        draft.loading = false;
        draft.error = true;
        draft.errorMessage = action.error;
        break;
      case SEARCH_PARKINGLOT:
        draft.loading = true;
        draft.error = false;
        draft.errorMessage = '';
        draft.parking.search = {}
        break;
      case SEARCH_PARKINGLOT_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.errorMessage = '';
        draft.parking.search = action.searchParkingLot
        break;
      case SEARCH_PARKINGLOT_ERROR:
        draft.loading = false;
        draft.error = true;
        draft.errorMessage = action.error;
        break;
      case RETRIEVE_PARKING:
        draft.loading = true;
        draft.error = false;
        draft.errorMessage = '';
        break;
      case RETRIEVE_PARKING_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.errorMessage = '';
        draft.parking.list = action.retrieveParking;
        break;
      case RETRIEVE_PARKING_ERROR:
        draft.loading = false;
        draft.error = true;
        draft.errorMessage = action.error;
        break;
      case RETRIEVE_RESERVATIONS:
        draft.loading = true;
        draft.error = false;
        draft.errorMessage = '';
        break;
      case RETRIEVE_RESERVATIONS_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.errorMessage = '';
        draft.reservation.list = action.retrieveReservation;
        break;
      case RETRIEVE_RESERVATIONS_ERROR:
        draft.loading = false;
        draft.error = true;
        draft.errorMessage = action.error;
        break;
      case DELETE_ALL_RESERVATION:
        draft.loading = true;
        draft.error = false;
        draft.errorMessage = '';
        break;
      case DELETE_ALL_RESERVATION_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.errorMessage = '';
        break;
      case DELETE_ALL_RESERVATION_ERROR:
        draft.loading = false;
        draft.error = true;
        draft.errorMessage = action.error;
        break;
      case CREATE_RESERVATION:
        draft.loading = true;
        draft.error = false;
        draft.errorMessage = '';
        draft.reservation.create = {};
        break;
      case CREATE_RESERVATION_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.errorMessage = '';
        draft.reservation.create = action.createReservation
        break;
      case CREATE_RESERVATION_ERROR:
        draft.loading = false;
        draft.error = true;
        draft.errorMessage = action.error;
        break;
    }
  });

export default homeReducer;
