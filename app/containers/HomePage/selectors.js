/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.loading,
  );

const makeSelectError = () =>
  createSelector(
    selectHome,
    homeState => homeState.error,
  );

const makeSelectErrorMessage = () =>
  createSelector(
    selectHome,
    homeState => homeState.errorMessage,
  );

const makeSelectParking = () =>
  createSelector(
    selectHome,
    homeState => homeState.parking,
  );

const makeSelectReservation = () =>
  createSelector(
    selectHome,
    homeState => homeState.reservation,
  );


export {
  selectHome,
  makeSelectLoading,
  makeSelectError,
  makeSelectErrorMessage,
  makeSelectParking,
  makeSelectReservation,
};
