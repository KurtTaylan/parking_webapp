/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useTable } from 'react-table';


import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import H2 from 'components/H2';
import CenteredSection from './CenteredSection';
import Section from './Section';

import {
  retrieveParking,
  retrieveReservation,
  deleteAllReservation,
  createParkinglot,
  createReservation,
  searchParkinglot,
} from './actions';

import {
  makeSelectLoading,
  makeSelectError,
  makeSelectErrorMessage,
  makeSelectParking,
  makeSelectReservation,
} from './selectors';

import reducer from './reducer';
import saga from './saga';
import H3 from '../../components/H3';
import LoadingIndicator from '../../components/LoadingIndicator';


const key = 'home';

const Styles = styled.div`
  padding: 1rem;
  table {
    border-spacing: 0;
    border: 1px solid black;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }
    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      :last-child {
        border-right: 0;
      }
    }
  }
`;

export function HomePage({
                           loading,
                           error,
                           errorMessage,
                           parking,
                           reservation,
                           retrieveParkingList,
                           retrieveReservationList,
                           deleteAllReservation,
                           onSubmitCreateParkingLotForm,
                           onSubmitSearchParkingLotForm,
                           onSubmitCreateReservationForm,
                         }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    retrieveParkingList();
    retrieveReservationList();
  }, []);

  const {
    register: register1,
    errors: errors1,
    handleSubmit: handleSubmit1,
  } = useForm();

  const {
    register: register2,
    errors: errors2,
    handleSubmit: handleSubmit2,
  } = useForm();

  const {
    register: register3,
    errors: errors3,
    handleSubmit: handleSubmit3,
  } = useForm();

  const onCreateParkingLotSubmit = data => onSubmitCreateParkingLotForm(data);
  const onSearchParkingLotSubmit = data => {
    onSubmitSearchParkingLotForm({
      parkingID: data.lotParkingID,
      date: data.lotDate,
      startTime: data.lotStartTime,
    });
  };
  const onCreateReservationSubmit = data => {
    onSubmitCreateReservationForm({
      parkingID: data.reservationParkingID,
      date: data.reservationDate,
      startTime: data.reservationStartTime,
      phoneNumber: data.reservationPhoneNumber,
    });
  };

  const parkingColumns = React.useMemo(
    () => [
      {
        Header: 'Parking',
        columns: [
          {
            Header: 'ID',
            accessor: 'parkingID',
          },
          {
            Header: 'Name',
            accessor: 'parkingName',
          },
          {
            Header: 'Lot ID',
            accessor: 'parkingLotID',
          },
          {
            Header: 'Lot Name',
            accessor: 'parkingLotName',
          },
        ],
      },
    ],
    []);

  const parkingData = (data) => {
    let convertedParkingDataList = [];
    data.map(parkingElement => {
      parkingElement.parkingLotList.map(parkingLotElement => {
        convertedParkingDataList.push({
          parkingID: parkingElement.parkingId,
          parkingName: parkingElement.parkingName,
          parkingLotID: parkingLotElement.parkingLotId,
          parkingLotName: parkingLotElement.parkingLotName,
        });
      });
    });

    return convertedParkingDataList;
  };

  const reservationColumns = React.useMemo(
    () => [
      {
        Header: 'Reservations',
        columns: [
          {
            Header: 'ID',
            accessor: 'id',
          },
          {
            Header: 'Cost',
            accessor: 'cost',
          },
          {
            Header: 'Phone Number',
            accessor: 'phoneNumber',
          },
          {
            Header: 'Order Time',
            accessor: 'orderTime',
          },
          {
            Header: 'Start Time',
            accessor: 'startTime',
          },
          {
            Header: 'Ending Time',
            accessor: 'endingTime',
          },
          {
            Header: 'Parking Name',
            accessor: 'parkingName',
          },
          {
            Header: 'Parking Lot Name',
            accessor: 'parkingLotName',
          },
        ],
      },
    ],
    []);

  const reservationData = (data) => {
    let convertedReservationDataList = [];
    data.map(reservationElement => {
      convertedReservationDataList.push({
        id: reservationElement.id,
        phoneNumber: reservationElement.phoneNumber,
        cost: reservationElement.cost,
        orderTime: reservationElement.orderTime,
        startTime: reservationElement.startTime,
        endingTime: reservationElement.endingTime,
        parkingName: reservationElement.parkingName,
        parkingLotName: reservationElement.parkingLotName,
      });
    });

    return convertedReservationDataList;
  };

  const TableComponent = ({ columns, data }) => {
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      rows,
      prepareRow,
    } = useTable({
      columns,
      data,
    });

    // Render the UI for your table
    return (
      <table {...getTableProps()}>
        <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
        </thead>
        <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>;
              })}
            </tr>
          );
        })}
        </tbody>
      </table>
    );
  };


  return (
    <article>
      <Helmet
        titleTemplate="%s - Parking"
        defaultTitle="Parking"
      >
        <meta name="description" content="Parking Challenge Project"/>
      </Helmet>
      <div>
        <CenteredSection>
          <H2>
            Parking Web Application
          </H2>
          <p>
            You can List, Create, Search Parkings and Make Reservations
          </p>
        </CenteredSection>
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'baseline',
        }}>
          <Section>
            <H2>
              List of Parkings
            </H2>
            <Styles>
              <div>
                <TableComponent columns={parkingColumns} data={parkingData(parking.list)}/>
                {loading ? <LoadingIndicator/> : <button
                  style={{
                    marginTop: `16px`,
                  }}
                  onClick={event => {
                    event.preventDefault();
                    retrieveParkingList();
                  }}
                >
                  Update
                </button>}
              </div>
            </Styles>
          </Section>
          <div style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'baseline',
          }}>
            <Section>
              <H2>
                Create Parking Lot
              </H2>
              <Styles>
                <form key={1} style={{ display: 'flex', flexDirection: 'column' }}
                      onSubmit={handleSubmit1(onCreateParkingLotSubmit)}>
                  <input name="parkingID" ref={register1({ required: true })}
                         placeholder={'Parking ID, Enter Like Following: 1'} style={{ width: '350px' }}/>
                  {errors1.parkingId && <span>This field is required</span>}

                  <input name="parkingLotName" ref={register1({ required: true })}
                         placeholder={'Parking Lot Name, Enter Any Input'}/>
                  {errors1.parkingId && <span>This field is required</span>}
                  {loading ? <LoadingIndicator/> : <input type="submit"/>}
                </form>
              </Styles>
            </Section>
            <Section>
              <H2>
                Result:
              </H2>
              <Styles>
                <div style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'baseline',
                }}>
                  {
                    !loading && parking.create && parking.create.parkingLotID &&
                    <div>
                      <H3>
                        Parking Lot is Created
                      </H3>
                      <div>
                        <div style={{ fontWeight: 'bold' }}>Parking Lot ID:</div>
                        {parking.create.parkingLotID} </div>
                    </div>
                  }
                  {
                    !loading && error && errorMessage && <div>
                      <div style={{ fontWeight: 'bold' }}>{errorMessage}</div>
                    </div>
                  }
                </div>
              </Styles>
            </Section>
          </div>
        </div>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
      }}>
        <Section>
          <H2>
            Search Available Parking Lot:
          </H2>
          <Styles>
            <form key={2} style={{ display: 'flex', flexDirection: 'column' }}
                  onSubmit={handleSubmit2(onSearchParkingLotSubmit)}>
              <input name="lotParkingID" ref={register2({ required: true })}
                     placeholder={'Parking ID, Enter Like Following: 1'} style={{ width: `450px` }}/>
              {errors2.lotParkingID && <span>This field is required</span>}

              <input name="lotDate" ref={register2({ required: true })}
                     placeholder={'Date, Enter Like Following: 2020-06-06'}/>
              {errors2.lotDate && <span>This field is required</span>}

              <input name="lotStartTime" ref={register2({ required: true })}
                     placeholder={'Start Time, Enter Time with 15 minutes inverval: 14:15'}/>
              {errors2.lotStartTime && <span>This field is required</span>}
              {loading ? <LoadingIndicator/> : <input type="submit"/>}
            </form>
          </Styles>
        </Section>
        <Section>
          <H2>
            Result:
          </H2>
          <Styles>
            <div style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'baseline',
            }}>
              {
                !loading && parking.search && parking.search.selectedParkingLotAvailability &&
                <div>
                  <H3>
                    Parking is Available on Searched Criteria
                  </H3>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>ID:</div>
                    {parking.search.selectedParkingLotAvailability.parkingID} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Parking:</div>
                    {parking.search.selectedParkingLotAvailability.parkingName} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Lot:</div>
                    {parking.search.selectedParkingLotAvailability.parkingLotName} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Time:</div>
                    {parking.search.selectedParkingLotAvailability.slotAvailabilityStartTime} </div>
                </div>
              }
              {
                !loading && parking.search && parking.search.nextBestAvailableOption &&
                <div>
                  <H3>
                    Parking NOT Available on Searched Criteria, Next Best Available Lot
                  </H3>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>ID:</div>
                    {parking.search.nextBestAvailableOption.parkingID} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Parking:</div>
                    {parking.search.nextBestAvailableOption.parkingName} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Lot:</div>
                    {parking.search.nextBestAvailableOption.parkingLotName} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Time:</div>
                    {parking.search.nextBestAvailableOption.slotAvailabilityStartTime} </div>
                </div>
              }
              {
                !loading && error && errorMessage && <div>
                  <div style={{ fontWeight: 'bold' }}>{errorMessage}</div>
                </div>
              }
            </div>
          </Styles>
        </Section>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
      }}>
        <Section>
          <H2>
            Create Reservation
          </H2>
          <Styles>
            <form key={3} style={{ display: 'flex', flexDirection: 'column' }}
                  onSubmit={handleSubmit3(onCreateReservationSubmit)}>
              <input name="reservationParkingID" ref={register3({ required: true })}
                     placeholder={'Parking ID, Enter Like Following: 1'} style={{ width: `450px` }}/>
              {errors3.reservationParkingID && <span>This field is required</span>}

              <input name="reservationPhoneNumber" ref={register3({ required: true })}
                     placeholder={'Phone Number, Any Input, No Phone Number Regex Check'}/>
              {errors3.reservationPhoneNumber && <span>This field is required</span>}

              <input name="reservationDate" ref={register3({ required: true })}
                     placeholder={'Date, Enter Like Following: 2020-06-06'}/>
              {errors3.reservationDate && <span>This field is required</span>}

              <input name="reservationStartTime" ref={register3({ required: true })}
                     placeholder={'Start Time: Enter Time with 15 minutes inverval: 14:15'}/>
              {errors3.reservationStartTime && <span>This field is required</span>}
              {loading ? <LoadingIndicator/> : <input type="submit"/>}
            </form>
          </Styles>
        </Section>
        <Section>
          <H2>
            Result:
          </H2>
          <Styles>
            <div style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'baseline',
            }}>
              {
                !loading && reservation.create && reservation.create.reservationID &&
                <div>
                  <H3>
                    Reservation is Created
                  </H3>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>ID:</div>
                    {reservation.create.reservationID} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Parking:</div>
                    {reservation.create.parkName} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Lot:</div>
                    {reservation.create.parkLotName} </div>
                  <div>
                    <div style={{ fontWeight: 'bold' }}>Time:</div>
                    {reservation.create.reservationStartTime} </div>
                </div>
              }
              {
                !loading && error && errorMessage && <div>
                  <div style={{ fontWeight: 'bold' }}>{errorMessage}</div>
                </div>
              }
            </div>
          </Styles>
        </Section>
      </div>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
      }}>
        <Section>
          <H2>
            List of Reservations
          </H2>
          <Styles>
            <div>
              <TableComponent columns={reservationColumns} data={reservationData(reservation.list)}/>
              <div>
                {loading ? <LoadingIndicator/> : <div>
                  <button
                    style={{
                      marginTop: `16px`,
                    }}
                    onClick={event => {
                      event.preventDefault();
                      retrieveReservationList();
                    }}
                  >
                    Update
                  </button>
                  <button
                    style={{
                      marginTop: `16px`,
                      marginLeft: `5px`,
                    }}
                    onClick={event => {
                      event.preventDefault();
                      deleteAllReservation();
                    }}
                  >
                    Clean All Reservations
                  </button>
                </div>
                }
              </div>
            </div>
          </Styles>
        </Section>
      </div>
    </article>
  );
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  errorMessage: makeSelectErrorMessage(),
  parking: makeSelectParking(),
  reservation: makeSelectReservation(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitCreateParkingLotForm: data => {
      dispatch(createParkinglot(data));
    },
    onSubmitCreateReservationForm: data => {
      dispatch(createReservation(data));
    },
    onSubmitSearchParkingLotForm: data => {
      dispatch(searchParkinglot(data));
    },
    retrieveParkingList: () => {
      dispatch(retrieveParking());
    },
    retrieveReservationList: () => {
      dispatch(retrieveReservation());
    },
    deleteAllReservation: () => {
      dispatch(deleteAllReservation());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
