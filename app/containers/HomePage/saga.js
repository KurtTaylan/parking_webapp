/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';

import {
  RETRIEVE_PARKING,
  CREATE_PARKINGLOT,
  SEARCH_PARKINGLOT,
  RETRIEVE_RESERVATIONS,
  CREATE_RESERVATION,
  DELETE_ALL_RESERVATION,
} from './constants';

import {
  retrieveParkingSuccess,
  retrieveParkingError,

  createParkinglotSuccess,
  createParkinglotError,

  searchParkinglotSuccess,
  searchParkinglotError,

  retrieveReservationSuccess,
  retrieveReservationError,

  createReservationSuccess,
  createReservationError,

  deleteAllReservationSuccess,
  deleteAllReservationError,
} from './actions';

export function* retrieveParkingSaga() {
  try {
    const apiCall = () => {
      return axios.get('https://park-test.herokuapp.com/api/v1/parkings')
        .then(response => response)
        .catch(err => err.response);
    };

    const response = yield call(apiCall);
    if (response && 200 === response.status) {
      yield put(retrieveParkingSuccess(response.data.parkingList));
    } else if (response && 400 === response.status) {
      yield put(retrieveParkingError(response.data.message));
    } else {
      yield put(retrieveParkingError('Unexpected Application Behaviour'));
    }
  } catch (err) {
    console.error(err);
    yield put(retrieveParkingError(err));
  }
}

export function* createParkinglotSaga(action) {
  try {
    const { parkingID, parkingLotName } = action;
    const apiCall = () => {
      return axios.post('https://park-test.herokuapp.com/api/v1/parkings', {
        parkingID,
        parkingLotName,
      }).then(response => response)
        .catch(err => err.response);
    };

    const response = yield call(apiCall);
    if (response && 200 === response.status) {
      yield put(createParkinglotSuccess(response.data));
    } else if (response && 400 === response.status) {
      console.log(response)
      yield put(createParkinglotError(response.data.message));
    } else {
      yield put(createParkinglotError('Unexpected Application Behaviour'));
    }
  } catch (err) {
    yield put(createParkinglotError(err));
  }
}

export function* searchParkinglotSaga(action) {
  try {
    const { parkingID, date, startTime } = action;
    const apiCall = () => {
      return axios.get('https://park-test.herokuapp.com/api/v1/parkings/availabilities', {
        params: {
          parkingID,
          date,
          startTime,
        },
      }).then(response => response)
        .catch(err => err.response);
    };

    const response = yield call(apiCall);
    if (response && 200 === response.status) {
      yield put(searchParkinglotSuccess(response.data));
    } else if (response && 400 === response.status) {
      yield put(searchParkinglotError(response.data.message));
    } else {
      yield put(searchParkinglotError('Unexpected Application Behaviour'));
    }
  } catch (err) {
    yield put(searchParkinglotError(err));
  }
}

export function* retrieveReservationSaga() {
  try {
    const apiCall = () => {
      return axios.get('https://park-test.herokuapp.com/api/v1/reservations')
        .then(response => response)
        .catch(err => err.response);
    };

    const response = yield call(apiCall);
    if (response && 200 === response.status) {
      yield put(retrieveReservationSuccess(response.data.reservationList));
    } else if (response && 400 === response.status) {
      yield put(retrieveReservationError(response.data.message));
    } else {
      yield put(retrieveReservationError('Unexpected Application Behaviour'));
    }
  } catch (err) {console.error(err);
    yield put(retrieveReservationError(err));
  }
}

export function* createReservationSaga(action) {
  try {
    const { parkingID, phoneNumber, date, startTime } = action;
    console.log(parkingID, phoneNumber, date, startTime);
    const apiCall = () => {
      return axios.post('https://park-test.herokuapp.com/api/v1/reservations', {
        parkingID,
        phoneNumber,
        date,
        startTime,
      })
        .then(response => response)
        .catch(err => err.response);
    };

    const response = yield call(apiCall);
    if (response && 200 === response.status) {
      yield put(createReservationSuccess(response.data));
    } else if (response && 400 === response.status) {
      yield put(createReservationError(response.data.message));
    } else {
      yield put(createReservationError('Unexpected Application Behaviour'));
    }
  } catch (err) {
    console.log('Catch error: ', err);
    yield put(createReservationError(err));
  }
}

export function* deleteAllReservationSaga() {
  try {
    const apiCall = () => {
      return axios.delete('https://park-test.herokuapp.com/api/v1/reservations')
        .then(response => response)
        .catch(err => err.response);
    };

    const response = yield call(apiCall);
    if (response && 200 === response.status) {
      yield put(deleteAllReservationSuccess());
    } else if (response && 400 === response.status) {
      yield put(deleteAllReservationError(response.data.message));
    } else {
      yield put(deleteAllReservationError('Unexpected Application Behaviour'));
    }
  } catch (err) {
    yield put(deleteAllReservationError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* homeData() {
  yield takeLatest(RETRIEVE_PARKING, retrieveParkingSaga);
  yield takeLatest(CREATE_PARKINGLOT, createParkinglotSaga);
  yield takeLatest(SEARCH_PARKINGLOT, searchParkinglotSaga);

  yield takeLatest(CREATE_RESERVATION, createReservationSaga);
  yield takeLatest(RETRIEVE_RESERVATIONS, retrieveReservationSaga);
  yield takeLatest(DELETE_ALL_RESERVATION, deleteAllReservationSaga);
}
